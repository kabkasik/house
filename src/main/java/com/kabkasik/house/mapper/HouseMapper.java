package com.kabkasik.house.mapper;

import com.kabkasik.house.dto.HouseDto;
import com.kabkasik.house.entity.House;
import org.mapstruct.Mapper;

@Mapper(uses = PeopleMapper.class)
public interface HouseMapper {
    HouseDto map(House house);
    House map(HouseDto houseDto);
}
