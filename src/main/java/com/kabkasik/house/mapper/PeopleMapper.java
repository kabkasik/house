package com.kabkasik.house.mapper;

import com.kabkasik.house.dto.PeopleDto;
import com.kabkasik.house.entity.People;
import org.mapstruct.Mapper;

@Mapper
public interface PeopleMapper {
    PeopleDto map(People peoples);
    People map(PeopleDto peopleDto);
}
