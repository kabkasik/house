package com.kabkasik.house.controller;

import com.kabkasik.house.dto.ErrorDto;
import com.kabkasik.house.dto.HouseDto;
import com.kabkasik.house.dto.RegistrationDto;
import com.kabkasik.house.dto.ResponseDto;
import com.kabkasik.house.exceptions.HouseException;
import com.kabkasik.house.service.HouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController()
@RequestMapping("/api/v1/house")
@RequiredArgsConstructor
@Slf4j
public class HouseController {

    private final HouseService houseService;
    private final MessageSource messageSource;

    @GetMapping(value = "{houseId}")
    private ResponseDto<HouseDto> get(@PathVariable Long houseId) {
        return ResponseDto.success(houseService.get(houseId));
    }

    @GetMapping(value = "all")
    private ResponseDto<List<HouseDto>> getAll() {
        return ResponseDto.success(houseService.getAll());
    }

    @PostMapping()
    private ResponseDto<HouseDto> add(@RequestBody @Valid RegistrationDto registrationDto)  {
        return ResponseDto.success(houseService.registration(registrationDto));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HouseException.class)
    public ResponseDto<?> handleValidationCommonExceptions(HouseException ex) {
        String code = ex.getCode();
        return ResponseDto.failed(
                Stream.of(createErrorDto(code, ex.getObjects(), code, true))
                        .collect(Collectors.toList()));
    }

    private ErrorDto createErrorDto(String code, Object[] objects, String defaultMessage, Boolean isGenError) {
        String message = messageSource.getMessage(code, objects, defaultMessage, LocaleContextHolder.getLocale());
        if (isGenError && message == defaultMessage) {
            log.error("createErrorDto: Failed to get localization with: {}", code);
        }
        return ErrorDto.of(
                message,
                code);
    }

    private ErrorDto createErrorDto(String code, Object[] objects, String defaultMessage) {
        return createErrorDto(code, objects, defaultMessage, false);
    }

}
