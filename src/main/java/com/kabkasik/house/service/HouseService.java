package com.kabkasik.house.service;

import com.kabkasik.house.dto.HouseDto;
import com.kabkasik.house.dto.RegistrationDto;
import com.kabkasik.house.entity.House;
import com.kabkasik.house.entity.People;
import com.kabkasik.house.exceptions.HouseException;
import com.kabkasik.house.mapper.HouseMapper;
import com.kabkasik.house.repository.HouseRepository;
import com.kabkasik.house.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class HouseService {

    private final HouseRepository houseRepository;
    private final HouseMapper houseMapper;

    @Transactional
    public HouseDto get(Long houseId) {
        log.debug("get: Starting with houseId:{}", houseId);
        try {

            HouseDto houseDto = houseRepository.findById(houseId)
                    .map(houseMapper::map)
                    .orElseThrow(() -> HouseException.of("house not found", houseId));

            log.info("get: Finished with houseId:{}", houseId);
            return houseDto;
        } catch (Exception oO) {
            log.error(String.format("get: Failed with houseId:{}", houseId), oO);
            throw HouseException.of("house not found", houseId);
        }
    }

    @Transactional
    public List<HouseDto> getAll() {
        log.debug("get: Starting");
        try {

            List<HouseDto> collect = houseRepository.findAll().stream()
                    .map(houseMapper::map)
                    .collect(Collectors.toList());


            log.info("get: Finished");
            return collect;
        } catch (Exception oO) {
            log.error("get: Failed", oO);
            throw HouseException.of("error");
        }
    }

    @Transactional
    public HouseDto registration(RegistrationDto registrationDto) {
        log.debug("registration: Starting with registrationDto:{}", registrationDto);
        try {

            House house = houseRepository.findById(registrationDto.getHouseId())
                    .orElse(new House(registrationDto.getHouseId(), new ArrayList<>()));
            house.getPeoples().add(new People(null, registrationDto.getPeopleName()));
            HouseDto houseDto = houseMapper.map(houseRepository.save(house));

            log.info("registration: Finished with registrationDto:{}", registrationDto);
            return houseDto;
        } catch (Exception oO) {
            log.error(String.format("registration: Failed with registrationDto:{}", registrationDto), oO);
            throw HouseException.of("error");
        }
    }
}
