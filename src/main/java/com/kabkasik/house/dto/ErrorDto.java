package com.kabkasik.house.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto {

    @ApiModelProperty(notes = "Описание ошибки")
    String message;

    @ApiModelProperty(notes = "Код ошибки")
    String code;

    public static ErrorDto of(String message, String code){
        return new ErrorDto(message, code);
    }
}
