package com.kabkasik.house.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {

    @ApiModelProperty(notes = "Объект хранит результат запроса")
    T data;

    @ApiModelProperty(notes = "Список ошибок")
    List<ErrorDto> errors;

    public static <T> ResponseDto success(T data) {
        return new ResponseDto(data, null);
    }

    public static <Void> ResponseDto success() {
        return new ResponseDto(null, null);
    }

    public static ResponseDto failed(List<ErrorDto> errors) {
        return new ResponseDto(null, errors);
    }

}
