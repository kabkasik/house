package com.kabkasik.house.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HouseDto {
    private Long id;
    private List<PeopleDto> peoples = new ArrayList<>();
}
