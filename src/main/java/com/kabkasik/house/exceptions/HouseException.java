package com.kabkasik.house.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class HouseException extends RuntimeException{

    private static final long serialVersionUID = 6826616801241472481L;

    private final String code;
    private final Object[] objects;

    public String getCode() {
        return code;
    }

    public Object[] getObjects() {
        return objects;
    }

    public static HouseException of(String code, Object... objects) {
        return new HouseException(code, objects);
    }
}
