CREATE TABLE house
(
    id BIGSERIAL PRIMARY KEY
);

CREATE TABLE people
(
    id       BIGSERIAL PRIMARY KEY,
    name     VARCHAR NOT NULL UNIQUE,
    house_id BIGINT REFERENCES house (id)

);
CREATE INDEX people_master_of_puppets_id_idx ON people (house_id);
