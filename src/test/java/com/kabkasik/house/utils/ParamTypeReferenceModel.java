package com.kabkasik.house.utils;

import com.kabkasik.house.dto.HouseDto;
import com.kabkasik.house.dto.ResponseDto;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;
import java.util.UUID;

public class ParamTypeReferenceModel {

    public static final ParameterizedTypeReference<ResponseDto<Void>> _void =
            new ParameterizedTypeReference<ResponseDto<Void>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<UUID>> _uuid =
            new ParameterizedTypeReference<ResponseDto<UUID>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<HouseDto>> _house =
            new ParameterizedTypeReference<ResponseDto<HouseDto>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<List<HouseDto>>> _houses =
            new ParameterizedTypeReference<ResponseDto<List<HouseDto>>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<Long>> _long =
            new ParameterizedTypeReference<ResponseDto<Long>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<String>> _string =
            new ParameterizedTypeReference<ResponseDto<String>>() {
            };
    public static final ParameterizedTypeReference<ResponseDto<Boolean>> _bool =
            new ParameterizedTypeReference<ResponseDto<Boolean>>() {
            };

}

