package com.kabkasik.house.utils;

import com.kabkasik.house.dto.ResponseDto;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;

public class Utils {
    public static String loadFromResource(Class<?> testClass, String jsonFileName) {
        try (InputStream resourceAsStream = testClass.getResourceAsStream(jsonFileName)) {
            return IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8.name());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> T convert(ObjectMapper objectMapper, String json, Class<T> objectClass) {
        try {
            return objectMapper.readValue(json, objectClass);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> T getObjectFromResourceJson(Class<?> testClass, String jsonFileName, Class<T> objectClass){
        ObjectMapper objectMapper = new ObjectMapper();
        String loadFromResource = loadFromResource(testClass, jsonFileName);
        return convert(objectMapper, loadFromResource, objectClass);
    }

    public static <T> ResponseEntity<ResponseDto<T>> request(HttpMethod httpMethod, String url, Object requestObject, TestRestTemplate testRestTemplate, ParameterizedTypeReference<ResponseDto<T>> typeReference) {
        RequestEntity.BodyBuilder builderGet = RequestEntity
                .method(httpMethod, URI.create(url))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "ru");

        return testRestTemplate.exchange(builderGet.body(requestObject), typeReference);
    }
}

