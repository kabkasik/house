package com.kabkasik.house.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.kabkasik.house.dto.ErrorDto;
import com.kabkasik.house.dto.HouseDto;
import com.kabkasik.house.dto.ResponseDto;


public class TypeReferenceModel {

    public static final TypeReference<ResponseDto<HouseDto>> _houseResp =
            new TypeReference<ResponseDto<HouseDto>>() {
            };
    public static final TypeReference<ErrorDto> _errorDto =
            new TypeReference<ErrorDto>() {
            };
}
